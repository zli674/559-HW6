<!DOCTYPE html>
<!--
Created using JS Bin
http://jsbin.com

Copyright (c) 2017 by anonymous (http://jsbin.com/xilovuruqe/1/edit)

Released under the MIT license: http://jsbin.mit-license.org
-->
<meta name="robots" content="noindex">
<html>

  <head>
    <meta name="description" content="Spinning cube example">
    <meta charset="utf-8">
    <title>WebGL texturing w/mipmap example</title>

    <script id="vs" type="not-js">
      attribute vec3 vPosition;
      attribute vec3 vNormal;
      attribute vec3 vColor;
      attribute vec2 vTexCoord;
      varying vec3 fPosition;
      varying vec3 fColor;
      varying vec3 fNormal;
      varying vec2 fTexCoord;
      uniform mat4 uMV;
      uniform mat4 uMVP;

      void main(void) {
        gl_Position = uMVP * vec4(vPosition, 1.0);
        fPosition = (uMV * vec4(vPosition, 1.0)).xyz; // In camera coordinates
        fColor = vColor;
        fNormal = vNormal;                            // In local coordinates
        fTexCoord = vTexCoord;
      }
    </script>

    <script id="fs" type="not-js">
      precision highp float;
      varying vec3 fPosition;
      varying vec3 fColor;
      varying vec3 fNormal;
      varying vec2 fTexCoord;
      uniform mat4 uMVn;
      uniform sampler2D texSampler1;
      uniform sampler2D texSampler2;
      uniform sampler2D texSampler3;

      const vec3  lightV    = vec3(0.0,0.0,1.0);
      const float lightI    = 1.0;               // only for diffuse component
      const float ambientC  = 0.15;
      const float diffuseC  = 0.3;
      const float specularC = 1.0;
      const float specularE = 16.0;
      const vec3  lightCol  = vec3(1.0,1.0,1.0);
      const vec3  objectCol = vec3(1.0,0.6,0.0); // yellow-ish orange
      vec2 blinnPhongDir(vec3 lightDir, vec3 n, float lightInt, float Ka,
        float Kd, float Ks, float shininess) {
        vec3 s = normalize(lightDir);
        vec3 v = normalize(-fPosition);
        vec3 h = normalize(v+s);
        float diffuse = Ka + Kd * lightInt * max(0.0, dot(n, s));
        float spec =  Ks * pow(max(0.0, dot(n,h)), shininess);
        return vec2(diffuse, spec);
      }

      void main(void) {
        vec3 texColor=texture2D(texSampler1,fTexCoord).xyz;
        vec3 n = (uMVn * vec4(fNormal, 0.0)).xyz;
        vec3 ColorS  = blinnPhongDir(lightV,n,0.0   ,0.0,     0.0,     specularC,specularE).y*lightCol;
        vec3 ColorAD = blinnPhongDir(lightV,n,lightI,ambientC,diffuseC,0.0,      1.0      ).x*texColor;
        gl_FragColor = vec4(ColorAD+ColorS,1.0);
      }
</script>

  </head>

  <body onload="start()">
    <canvas id="mycanvas" width="500" height="500"></canvas><br>
    <input id="slider4" type="range" min="-100" max="100" />
    <input id="slider1" type="range" min="-100" max="100" />
    <input id="slider2" type="range" min="-100" max="100" />
    <input id="slider3" type="range" min="-100" max="100" />
    <input id="slider5" type="range" min="-100" max="100" />
    <script src="http://graphics.cs.wisc.edu/JS/twgl-full.min.js"></script>
  <script id="jsbin-javascript">
// draw a textured cube using WebGL
//
// written by sifakis on October 20, 2015

function start() { "use strict";

    // Get canvas, WebGL context, twgl.m4
    var canvas = document.getElementById("mycanvas");
    var gl = canvas.getContext("webgl");
    var m4 = twgl.m4;

    // Sliders at center
    var slider1 = document.getElementById('slider1');
    slider1.value = 0;
    var slider2 = document.getElementById('slider2');
    slider2.value = -100;
    var slider3 = document.getElementById('slider3');
    slider3.value = 0;
    var slider4 = document.getElementById('slider4');
    slider4.value = -100;
    var slider5 = document.getElementById('slider5');
    slider5.value = 0;

    // Read shader source
    var vertexSource = document.getElementById("vs").text;
    var fragmentSource = document.getElementById("fs").text;

    // Compile vertex shader
    var vertexShader = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShader,vertexSource);
    gl.compileShader(vertexShader);
    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
      alert(gl.getShaderInfoLog(vertexShader)); return null; }
    
    // Compile fragment shader
    var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShader,fragmentSource);
    gl.compileShader(fragmentShader);
    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
      alert(gl.getShaderInfoLog(fragmentShader)); return null; }
    
    // Attach the shaders and link
    var shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);
    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
      alert("Could not initialize shaders"); }
    gl.useProgram(shaderProgram);	    
    
    // with the vertex shader, we need to pass it positions
    // as an attribute - so set up that communication
    shaderProgram.PositionAttribute = gl.getAttribLocation(shaderProgram, "vPosition");
    gl.enableVertexAttribArray(shaderProgram.PositionAttribute);
    
    shaderProgram.NormalAttribute = gl.getAttribLocation(shaderProgram, "vNormal");
    gl.enableVertexAttribArray(shaderProgram.NormalAttribute);
    
    shaderProgram.ColorAttribute = gl.getAttribLocation(shaderProgram, "vColor");
    gl.enableVertexAttribArray(shaderProgram.ColorAttribute);
    
    shaderProgram.texcoordAttribute = gl.getAttribLocation(shaderProgram, "vTexCoord");
    gl.enableVertexAttribArray(shaderProgram.texcoordAttribute);
    
    // this gives us access to the matrix uniform
    shaderProgram.MVmatrix = gl.getUniformLocation(shaderProgram,"uMV");
    shaderProgram.MVNormalmatrix = gl.getUniformLocation(shaderProgram,"uMVn");
    shaderProgram.MVPmatrix = gl.getUniformLocation(shaderProgram,"uMVP");
    
    // Attach samplers to texture units
    shaderProgram.texSampler1 = gl.getUniformLocation(shaderProgram, "texSampler1");
    gl.uniform1i(shaderProgram.texSampler1, 0);
    shaderProgram.texSampler2 = gl.getUniformLocation(shaderProgram, "texSampler2");
    gl.uniform1i(shaderProgram.texSampler2, 1);
    shaderProgram.texSampler3 = gl.getUniformLocation(shaderProgram, "texSampler3");
    gl.uniform1i(shaderProgram.texSampler3, 1);

    // Data ...
    
    // vertex positions
    var vertexPos_first = new Float32Array(
        [  1, 1, 1,  -1, 1, 1,  -1,-1, 1,   1,-1, 1,
           1, 1, 1,   1,-1, 1,   1,-1,-1,   1, 1,-1,
           1, 1, 1,   1, 1,-1,  -1, 1,-1,  -1, 1, 1,
          -1, 1, 1,  -1, 1,-1,  -1,-1,-1,  -1,-1, 1,
          -1,-1,-1,   1,-1,-1,   1,-1, 1,  -1,-1, 1,
           1,-1,-1,  -1,-1,-1,  -1, 1,-1,   1, 1,-1 ]);

    var vertexPos_second = new Float32Array(
       [  1, 0.1, 0.1,  -1, 0.1, 0.1,  -1,-0.1, 0.1,   1,-0.1, 0.1,
           1, 0.1, 0.1,   1,-0.1, 0.1,   1,-0.1,-0.1,   1, 0.1,-0.1,
           1, 0.1, 0.1,   1, 0.1,-0.1,  -1, 0.1,-0.1,  -1, 0.1, 0.1,
          -1, 0.1, 0.1,  -1, 0.1,-0.1,  -1,-0.1,-0.1,  -1,-0.1, 0.1,
          -1,-0.1,-0.1,   1,-0.1,-0.1,   1,-0.1, 0.1,  -1,-0.1, 0.1,
           1,-0.1,-0.1,  -1,-0.1,-0.1,  -1, 0.1,-0.1,   1, 0.1,-0.1 ]
        );
                  
    var vertexPos_third = new Float32Array(
       [  1, 0.1, 0.1,  -1, 0.1, 0.1,  -1,-0.1, 0.1,   1,-0.1, 0.1,
           1, 0.1, 0.1,   1,-0.1, 0.1,   1,-0.1,-0.1,   1, 0.1,-0.1,
           1, 0.1, 0.1,   1, 0.1,-0.1,  -1, 0.1,-0.1,  -1, 0.1, 0.1,
          -1, 0.1, 0.1,  -1, 0.1,-0.1,  -1,-0.1,-0.1,  -1,-0.1, 0.1,
          -1,-0.1,-0.1,   1,-0.1,-0.1,   1,-0.1, 0.1,  -1,-0.1, 0.1,
           1,-0.1,-0.1,  -1,-0.1,-0.1,  -1, 0.1,-0.1,   1, 0.1,-0.1 ]
        );

    var vertexPos_fourth = new Float32Array(
       [  0.1, 0.4, 0.1,  -0.1, 0.4, 0.1,  -0.1,-0.4, 0.1,   0.1,-0.4, 0.1,
           0.1, 0.4, 0.1,   0.1,-0.4, 0.1,   0.1,-0.4,-0.1,   0.1, 0.4,-0.1,
           0.1, 0.4, 0.1,   0.1, 0.4,-0.1,  -0.1, 0.4,-0.1,  -0.1, 0.4, 0.1,
          -0.1, 0.4, 0.1,  -0.1, 0.4,-0.1,  -0.1,-0.4,-0.1,  -0.1,-0.4, 0.1,
          -0.1,-0.4,-0.1,   0.1,-0.4,-0.1,   0.1,-0.4, 0.1,  -0.1,-0.4, 0.1,
           0.1,-0.4,-0.1,  -0.1,-0.4,-0.1,  -0.1, 0.4,-0.1,   0.1, 0.4,-0.1 ]);
                  
    var vertexPos_fifth = new Float32Array(
        [  1, 1, 1,  -1, 1, 1,  -1,-1, 1,   1,-1, 1,
           1, 1, 1,   1,-1, 1,   1,-1,-1,   1, 1,-1,
           1, 1, 1,   1, 1,-1,  -1, 1,-1,  -1, 1, 1,
          -1, 1, 1,  -1, 1,-1,  -1,-1,-1,  -1,-1, 1,
          -1,-1,-1,   1,-1,-1,   1,-1, 1,  -1,-1, 1,
           1,-1,-1,  -1,-1,-1,  -1, 1,-1,   1, 1,-1 ]);
        
    // vertex normals
    var vertexNormals_first = new Float32Array(
        [  0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1, 
           1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0, 
           0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0, 
          -1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0, 
           0,-1, 0,   0,-1, 0,   0,-1, 0,   0,-1, 0, 
           0, 0,-1,   0, 0,-1,   0, 0,-1,   0, 0,-1  ]);
                  
   var vertexNormals_second = new Float32Array(
        [  0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1, 
           1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0, 
           0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0, 
          -1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0, 
           0,-1, 0,   0,-1, 0,   0,-1, 0,   0,-1, 0, 
           0, 0,-1,   0, 0,-1,   0, 0,-1,   0, 0,-1  ]
        );
   var vertexNormals_third = new Float32Array(
        [  0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1, 
           1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0, 
           0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0, 
          -1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0, 
           0,-1, 0,   0,-1, 0,   0,-1, 0,   0,-1, 0, 
           0, 0,-1,   0, 0,-1,   0, 0,-1,   0, 0,-1  ]
        );
                  
   var vertexNormals_fourth = new Float32Array(
        [  0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1, 
           1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0, 
           0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0, 
          -1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0, 
           0,-1, 0,   0,-1, 0,   0,-1, 0,   0,-1, 0, 
           0, 0,-1,   0, 0,-1,   0, 0,-1,   0, 0,-1  ]
        );
                  
   var vertexNormals_fifth = new Float32Array(
        [  0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1, 
           1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0, 
           0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0, 
          -1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0, 
           0,-1, 0,   0,-1, 0,   0,-1, 0,   0,-1, 0, 
           0, 0,-1,   0, 0,-1,   0, 0,-1,   0, 0,-1  ]
        );

    // vertex colors
    var vertexColors_first = new Float32Array(
        [  0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1,
           1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0,
           0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0,
           1, 1, 0,   1, 1, 0,   1, 1, 0,   1, 1, 0,
           1, 0, 1,   1, 0, 1,   1, 0, 1,   1, 0, 1,
           0, 1, 1,   0, 1, 1,   0, 1, 1,   0, 1, 1 ]);
    
    var vertexColors_second = new Float32Array(
        [  0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1,
           1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0,
           0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0,
           1, 1, 0,   1, 1, 0,   1, 1, 0,   1, 1, 0,
           1, 0, 1,   1, 0, 1,   1, 0, 1,   1, 0, 1,
           0, 1, 1,   0, 1, 1,   0, 1, 1,   0, 1, 1 ]);
                  
    var vertexColors_third = new Float32Array(
        [  0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1,
           1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0,
           0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0,
           1, 1, 0,   1, 1, 0,   1, 1, 0,   1, 1, 0,
           1, 0, 1,   1, 0, 1,   1, 0, 1,   1, 0, 1,
           0, 1, 1,   0, 1, 1,   0, 1, 1,   0, 1, 1 ]);
                  
    var vertexColors_fourth = new Float32Array(
        [  0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1,
           1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0,
           0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0,
           1, 1, 0,   1, 1, 0,   1, 1, 0,   1, 1, 0,
           1, 0, 1,   1, 0, 1,   1, 0, 1,   1, 0, 1,
           0, 1, 1,   0, 1, 1,   0, 1, 1,   0, 1, 1 ]);

    var vertexColors_fifth = new Float32Array(
        [  0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1,
           1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0,
           0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0,
           1, 1, 0,   1, 1, 0,   1, 1, 0,   1, 1, 0,
           1, 0, 1,   1, 0, 1,   1, 0, 1,   1, 0, 1,
           0, 1, 1,   0, 1, 1,   0, 1, 1,   0, 1, 1 ]);
    // vertex texture coordinates
    var vertexTextureCoords = new Float32Array(
        [  0, 0,   1, 0,   1, 1,   0, 1,
           1, 0,   1, 1,   0, 1,   0, 0,
           0, 1,   0, 0,   1, 0,   1, 1,
           0, 0,   1, 0,   1, 1,   0, 1,
           1, 1,   0, 1,   0, 0,   1, 0,
           1, 1,   0, 1,   0, 0,   1, 0 ]);
    
    // element index array
    var triangleIndices_first = new Uint8Array(
        [  0, 1, 2,   0, 2, 3,    // front
           4, 5, 6,   4, 6, 7,    // right
           8, 9,10,   8,10,11,    // top
          12,13,14,  12,14,15,    // left
          16,17,18,  16,18,19,    // bottom
	      20,21,22,  20,22,23 ]); // back

    var triangleIndices_second = new Uint8Array(
        [  0, 1, 2,   0, 2, 3,    // front
           4, 5, 6,   4, 6, 7,    // right
           8, 9,10,   8,10,11,   // top
          12,13,14,  12,14,15,    // left
          16,17,18,  16,18,19,    // bottom
	      20,21,22,  20,22,23 ]); // back
                  
    var triangleIndices_third = new Uint8Array(
        [  0, 1, 2,   0, 2, 3,    // front
           4, 5, 6,   4, 6, 7,    // right
           8, 9,10,   8,10,11,   // top
          12,13,14,  12,14,15,    // left
          16,17,18,  16,18,19,    // bottom
	      20,21,22,  20,22,23 ]); // back
                  
    var triangleIndices_fourth = new Uint8Array(
        [  0, 1, 2,   0, 2, 3,    // front
           4, 5, 6,   4, 6, 7,    // right
           8, 9,10,   8,10,11,   // top
          12,13,14,  12,14,15,    // left
          16,17,18,  16,18,19,    // bottom
	      20,21,22,  20,22,23 ]); // back
                  
    var triangleIndices_fifth = new Uint8Array(
        [  0, 1, 2,   0, 2, 3,    // front
           4, 5, 6,   4, 6, 7,    // right
           8, 9,10,   8,10,11,   // top
          12,13,14,  12,14,15,    // left
          16,17,18,  16,18,19,    // bottom
	      20,21,22,  20,22,23 ]); // back
    // we need to put the vertices into a buffer so we can
    // block transfer them to the graphics hardware
    var trianglePosBuffer_first = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, trianglePosBuffer_first);
    gl.bufferData(gl.ARRAY_BUFFER, vertexPos_first, gl.STATIC_DRAW);
    trianglePosBuffer_first.itemSize = 3;
    trianglePosBuffer_first.numItems = 24;
    
    var trianglePosBuffer_second = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, trianglePosBuffer_second);
    gl.bufferData(gl.ARRAY_BUFFER, vertexPos_second, gl.STATIC_DRAW);
    trianglePosBuffer_second.itemSize = 3;
    trianglePosBuffer_second.numItems = 24;
                  
    var trianglePosBuffer_third = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, trianglePosBuffer_third);
    gl.bufferData(gl.ARRAY_BUFFER, vertexPos_third, gl.STATIC_DRAW);
    trianglePosBuffer_third.itemSize = 3;
    trianglePosBuffer_third.numItems = 24;
                  
    var trianglePosBuffer_fourth = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, trianglePosBuffer_fourth);
    gl.bufferData(gl.ARRAY_BUFFER, vertexPos_fourth, gl.STATIC_DRAW);
    trianglePosBuffer_fourth.itemSize = 3;
    trianglePosBuffer_fourth.numItems = 24;
                  
    var trianglePosBuffer_fifth = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, trianglePosBuffer_fifth);
    gl.bufferData(gl.ARRAY_BUFFER, vertexPos_fifth, gl.STATIC_DRAW);
    trianglePosBuffer_fifth.itemSize = 3;
    trianglePosBuffer_fifth.numItems = 24;

    // block transfer them to the graphics hardware
    var triangleNormalBuffer_first = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, triangleNormalBuffer_first);
    gl.bufferData(gl.ARRAY_BUFFER, vertexNormals_first, gl.STATIC_DRAW);
    triangleNormalBuffer_first.itemSize = 3;
    triangleNormalBuffer_first.numItems = 24;
                  
    var triangleNormalBuffer_second = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, triangleNormalBuffer_second);
    gl.bufferData(gl.ARRAY_BUFFER, vertexNormals_second, gl.STATIC_DRAW);
    triangleNormalBuffer_second.itemSize = 3;
    triangleNormalBuffer_second.numItems = 24;
                  
    var triangleNormalBuffer_third = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, triangleNormalBuffer_third);
    gl.bufferData(gl.ARRAY_BUFFER, vertexNormals_third, gl.STATIC_DRAW);
    triangleNormalBuffer_third.itemSize = 3;
    triangleNormalBuffer_third.numItems = 24;
                  
    var triangleNormalBuffer_fourth = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, triangleNormalBuffer_fourth);
    gl.bufferData(gl.ARRAY_BUFFER, vertexNormals_fourth, gl.STATIC_DRAW);
    triangleNormalBuffer_fourth.itemSize = 3;
    triangleNormalBuffer_fourth.numItems = 24;
    
    var triangleNormalBuffer_fifth = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, triangleNormalBuffer_fifth);
    gl.bufferData(gl.ARRAY_BUFFER, vertexNormals_fifth, gl.STATIC_DRAW);
    triangleNormalBuffer_fifth.itemSize = 3;
    triangleNormalBuffer_fifth.numItems = 24;
    // a buffer for colors
    var colorBuffer_first = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer_first);
    gl.bufferData(gl.ARRAY_BUFFER, vertexColors_first, gl.STATIC_DRAW);
    colorBuffer_first.itemSize = 3;
    colorBuffer_first.numItems = 24;

    var colorBuffer_second = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer_second);
    gl.bufferData(gl.ARRAY_BUFFER, vertexColors_second, gl.STATIC_DRAW);
    colorBuffer_second.itemSize = 3;
    colorBuffer_second.numItems = 24;
                  
    var colorBuffer_third = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer_third);
    gl.bufferData(gl.ARRAY_BUFFER, vertexColors_third, gl.STATIC_DRAW);
    colorBuffer_third.itemSize = 3;
    colorBuffer_third.numItems = 24;

    var colorBuffer_fourth = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer_fourth);
    gl.bufferData(gl.ARRAY_BUFFER, vertexColors_fourth, gl.STATIC_DRAW);
    colorBuffer_third.itemSize = 3;
    colorBuffer_third.numItems = 24;
                  
    var colorBuffer_fifth = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer_fifth);
    gl.bufferData(gl.ARRAY_BUFFER, vertexColors_fifth, gl.STATIC_DRAW);
    colorBuffer_fifth.itemSize = 3;
    colorBuffer_fifth.numItems = 24;
    // a buffer for textures
    var textureBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, textureBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, vertexTextureCoords, gl.STATIC_DRAW);
    textureBuffer.itemSize = 2;
    textureBuffer.numItems = 24;

    // a buffer for indices
    var indexBuffer_first = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer_first);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, triangleIndices_first, gl.STATIC_DRAW);    

    var indexBuffer_second = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer_second);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, triangleIndices_second, gl.STATIC_DRAW);   
    
    var indexBuffer_third = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer_third);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, triangleIndices_third, gl.STATIC_DRAW);    
                  
    var indexBuffer_fourth = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer_fourth);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, triangleIndices_fourth, gl.STATIC_DRAW); 
                  
    var indexBuffer_fifth = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer_fifth);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, triangleIndices_fifth, gl.STATIC_DRAW);

    // Set up texture
    var texture1 = gl.createTexture();
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture1);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
    var image1 = new Image();

    var texture2 = gl.createTexture();
    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, texture2);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
    var image2 = new Image();

    var texture3 = gl.createTexture();
    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, texture3);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
    var image3 = new Image();
                  
    function initTextureThenDraw()
    {
      image1.onload = function() { loadTexture(image1,texture1); };
      image1.crossOrigin = "anonymous";
      image1.src = "https://c1.staticflickr.com/5/4559/24452066758_64ed82b364_z.jpg";

      image2.onload = function() { loadTexture(image2,texture2); };
      image2.crossOrigin = "anonymous";
      image2.src = "https://c1.staticflickr.com/5/4579/38269063896_a9d7901d6d_z.jpg";

      image3.onload = function() { loadTexture(image3,texture3); };
      image3.crossOrigin = "anonymous";
      image3.src = "https://c1.staticflickr.com/5/4571/38324355091_c1f18c6187_z.jpg";
      window.setTimeout(draw,200);
    }


    function loadTexture(image,texture)
    {
      gl.bindTexture(gl.TEXTURE_2D, texture);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

      // Option 1 : Use mipmap, select interpolation mode
      gl.generateMipmap(gl.TEXTURE_2D);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);

      // Option 2: At least use linear filters
      // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);

      // Optional ... if your shader & texture coordinates go outside the [0,1] range
      // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    }    

    // Scene (re-)draw routine
    function draw() {
    
        // Translate slider values to angles in the [-pi,pi] interval
        var angle1 = slider1.value*0.01*Math.PI;
        var angle2 = 0.5*slider2.value*Math.PI * slider2.value*0.0001;
        var angle3 = slider3.value*0.01*Math.PI/2;
        var angle4 = slider5.value*0.01*Math.PI/8;
        var dx =slider4.value/100;
    
        // Circle around the y-axis
        var eye = [400*Math.sin(angle1),150.0,400.0*Math.cos(angle1)];
        var target = [0,0,0];
        var up = [0,1,0];

        
        var temp2_1 = m4.translate(m4.scaling([50,50,50]),[0,0,dx]);
        var temp2_3 = m4.translate(temp2_1, [1,0,0]);
        var temp2_2 = m4.multiply(m4.axisRotation([0,0,1],angle2),temp2_3);
        var tModel2 = m4.translate(temp2_2, [1,0,0]);
        var tnModel2 = m4.axisRotation([1,1,1],angle2);
              
        var temp3_1 = tModel2;
        var temp3_3 = m4.translate(temp3_1, [1,0,0]);
        var temp3_2 = m4.multiply(m4.axisRotation([0,0,1],-angle2*2),temp3_3);
        var tModel3 = m4.translate(temp3_2, [1,0,0]);
        var tnModel3 = m4.axisRotation([1,1,1],-angle2);
      
        var temp6_3 = m4.translate(temp2_1, [-1,0,0]);
        var temp6_2 = m4.multiply(m4.axisRotation([0,0,1],-angle2),temp6_3);
        var tModel6 = m4.translate(temp6_2, [-1,0,0]);
        var tnModel6 = m4.axisRotation([1,1,1],-angle2);
      
        var temp7_1 = tModel6;
        var temp7_3 = m4.translate(temp7_1, [-1,0,0]);
        var temp7_2 = m4.multiply(m4.axisRotation([0,0,1],angle2*2),temp7_3);
        var tModel7 = m4.translate(temp7_2, [-1,0,0]);
        var tnModel7 = m4.axisRotation([1,1,1],angle2);
      
        var tModel1 = m4.translate(m4.scaling([50,50,50]),[0,0,dx]);
        var tnModel1 = m4.axisRotation([1,1,1],0);
      
        var neckpos = m4.translate(m4.scaling([100,100,100]),[0,0,dx/2]);
        var temp4_1 = m4.translate(neckpos, [0,0.5,0]);
        var temp4_2 = m4.multiply(m4.axisRotation([0,1,0],angle3),temp4_1);
        var tModel4 = temp4_2;
        var tnModel4 = m4.axisRotation([1,1,1],0);
      
        var temp5_1 = m4.translate(m4.translate(m4.scaling([25,25,25]), [0,2,0]),[0,0,dx*2]);
        var temp5_2 = m4.translate(m4.multiply(m4.axisRotation([0,1,0],angle3),temp5_1), [0,2,0]);
        var tModel5 = m4.multiply(m4.axisRotation([1,0,0],angle4),temp5_2);
        var tnModel5 = m4.axisRotation([1,1,1],0);
      

        //var tModel5 = m4.scaling([100,100,50]);
        //var tnModel5 = m4.axisRotation([1,1,1],0);
      
        var tCamera = m4.inverse(m4.lookAt(eye,target,up));
        var tProjection = m4.perspective(Math.PI/3,1,10,1000);
    
        var tMV1=m4.multiply(tModel1,tCamera);
        var tMVn1=m4.multiply(tnModel1,tCamera);
        var tMVP1=m4.multiply(m4.multiply(tModel1,tCamera),tProjection);
      
        var tMV2=m4.multiply(tModel2,tCamera);
        var tMVn2=m4.multiply(tnModel2,tCamera);
        var tMVP2=m4.multiply(m4.multiply(tModel2,tCamera),tProjection);
      
        var tMV3=m4.multiply(tModel3,tCamera);
        var tMVn3=m4.multiply(tnModel3,tCamera);
        var tMVP3=m4.multiply(m4.multiply(tModel3,tCamera),tProjection);
      
        var tMV4=m4.multiply(tModel4,tCamera);
        var tMVn4=m4.multiply(tnModel4,tCamera);
        var tMVP4=m4.multiply(m4.multiply(tModel4,tCamera),tProjection);
      
        var tMV5=m4.multiply(tModel5,tCamera);
        var tMVn5=m4.multiply(tnModel5,tCamera);
        var tMVP5=m4.multiply(m4.multiply(tModel5,tCamera),tProjection);
    
        var tMV6=m4.multiply(tModel6,tCamera);
        var tMVn6=m4.multiply(tnModel6,tCamera);
        var tMVP6=m4.multiply(m4.multiply(tModel6,tCamera),tProjection);
      
        var tMV7=m4.multiply(tModel7,tCamera);
        var tMVn7=m4.multiply(tnModel7,tCamera);
        var tMVP7=m4.multiply(m4.multiply(tModel7,tCamera),tProjection);
        // Clear screen, prepare for rendering
        gl.clearColor(0.0, 0.0, 0.0, 1.0);
        gl.enable(gl.DEPTH_TEST);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    
        // Set up uniforms & attributes
        gl.uniformMatrix4fv(shaderProgram.MVmatrix,false,tMV1);
        gl.uniformMatrix4fv(shaderProgram.MVNormalmatrix,false,tMVn1);
        gl.uniformMatrix4fv(shaderProgram.MVPmatrix,false,tMVP1);
                 
        gl.bindBuffer(gl.ARRAY_BUFFER, trianglePosBuffer_first);
        gl.vertexAttribPointer(shaderProgram.PositionAttribute, trianglePosBuffer_first.itemSize,
          gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, triangleNormalBuffer_first);
        gl.vertexAttribPointer(shaderProgram.NormalAttribute, triangleNormalBuffer_first.itemSize,
          gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer_first);
        gl.vertexAttribPointer(shaderProgram.ColorAttribute, colorBuffer_first.itemSize,
          gl.FLOAT,false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, textureBuffer);
        gl.vertexAttribPointer(shaderProgram.texcoordAttribute, textureBuffer.itemSize,
          gl.FLOAT, false, 0, 0);

	    // Bind texture
        gl.activeTexture(gl.TEXTURE0);
        //gl.bindTexture(gl.TEXTURE_2D, texture1);
        //gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D, texture2);

	    // Do the drawing
        gl.drawElements(gl.TRIANGLES, triangleIndices_first.length, gl.UNSIGNED_BYTE, 0);
      
      
      

                    // Set up uniforms & attributes
        gl.uniformMatrix4fv(shaderProgram.MVmatrix,false,tMV2);
        gl.uniformMatrix4fv(shaderProgram.MVNormalmatrix,false,tMVn2);
        gl.uniformMatrix4fv(shaderProgram.MVPmatrix,false,tMVP2);
                 
        gl.bindBuffer(gl.ARRAY_BUFFER, trianglePosBuffer_second);
        gl.vertexAttribPointer(shaderProgram.PositionAttribute, trianglePosBuffer_second.itemSize,
          gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, triangleNormalBuffer_second);
        gl.vertexAttribPointer(shaderProgram.NormalAttribute, triangleNormalBuffer_second.itemSize,
          gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer_second);
        gl.vertexAttribPointer(shaderProgram.ColorAttribute, colorBuffer_second.itemSize,
          gl.FLOAT,false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, textureBuffer);
        gl.vertexAttribPointer(shaderProgram.texcoordAttribute, textureBuffer.itemSize,
          gl.FLOAT, false, 0, 0);

	    // Bind texture
        //gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texture1);
        gl.activeTexture(gl.TEXTURE1);
        //gl.bindTexture(gl.TEXTURE_2D, texture2);

	    // Do the drawing
        gl.drawElements(gl.TRIANGLES, triangleIndices_second.length, gl.UNSIGNED_BYTE, 0);

      
      

      
                    // Set up uniforms & attributes
        gl.uniformMatrix4fv(shaderProgram.MVmatrix,false,tMV3);
        gl.uniformMatrix4fv(shaderProgram.MVNormalmatrix,false,tMVn3);
        gl.uniformMatrix4fv(shaderProgram.MVPmatrix,false,tMVP3);
                 
        gl.bindBuffer(gl.ARRAY_BUFFER, trianglePosBuffer_third);
        gl.vertexAttribPointer(shaderProgram.PositionAttribute, trianglePosBuffer_third.itemSize,
          gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, triangleNormalBuffer_third);
        gl.vertexAttribPointer(shaderProgram.NormalAttribute, triangleNormalBuffer_third.itemSize,
          gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer_third);
        gl.vertexAttribPointer(shaderProgram.ColorAttribute, colorBuffer_third.itemSize,
          gl.FLOAT,false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, textureBuffer);
        gl.vertexAttribPointer(shaderProgram.texcoordAttribute, textureBuffer.itemSize,
          gl.FLOAT, false, 0, 0);

	    // Bind texture
        //gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texture1);
        gl.activeTexture(gl.TEXTURE1);
        //gl.bindTexture(gl.TEXTURE_2D, texture2);

	    // Do the drawing
        gl.drawElements(gl.TRIANGLES, triangleIndices_third.length, gl.UNSIGNED_BYTE, 0);
      
      
      
      
            
      
      
      
      
      
      
      
      
      
      
      gl.uniformMatrix4fv(shaderProgram.MVmatrix,false,tMV6);
        gl.uniformMatrix4fv(shaderProgram.MVNormalmatrix,false,tMVn6);
        gl.uniformMatrix4fv(shaderProgram.MVPmatrix,false,tMVP6);
                 
        gl.bindBuffer(gl.ARRAY_BUFFER, trianglePosBuffer_second);
        gl.vertexAttribPointer(shaderProgram.PositionAttribute, trianglePosBuffer_second.itemSize,
          gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, triangleNormalBuffer_second);
        gl.vertexAttribPointer(shaderProgram.NormalAttribute, triangleNormalBuffer_second.itemSize,
          gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer_second);
        gl.vertexAttribPointer(shaderProgram.ColorAttribute, colorBuffer_second.itemSize,
          gl.FLOAT,false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, textureBuffer);
        gl.vertexAttribPointer(shaderProgram.texcoordAttribute, textureBuffer.itemSize,
          gl.FLOAT, false, 0, 0);

	    // Bind texture
        //gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texture1);
        gl.activeTexture(gl.TEXTURE1);
        //gl.bindTexture(gl.TEXTURE_2D, texture2);

	    // Do the drawing
        gl.drawElements(gl.TRIANGLES, triangleIndices_second.length, gl.UNSIGNED_BYTE, 0);
      
      
      
      
      
      
      
      gl.uniformMatrix4fv(shaderProgram.MVmatrix,false,tMV7);
        gl.uniformMatrix4fv(shaderProgram.MVNormalmatrix,false,tMVn7);
        gl.uniformMatrix4fv(shaderProgram.MVPmatrix,false,tMVP7);
                 
        gl.bindBuffer(gl.ARRAY_BUFFER, trianglePosBuffer_third);
        gl.vertexAttribPointer(shaderProgram.PositionAttribute, trianglePosBuffer_third.itemSize,
          gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, triangleNormalBuffer_third);
        gl.vertexAttribPointer(shaderProgram.NormalAttribute, triangleNormalBuffer_third.itemSize,
          gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer_third);
        gl.vertexAttribPointer(shaderProgram.ColorAttribute, colorBuffer_third.itemSize,
          gl.FLOAT,false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, textureBuffer);
        gl.vertexAttribPointer(shaderProgram.texcoordAttribute, textureBuffer.itemSize,
          gl.FLOAT, false, 0, 0);

	    // Bind texture
        //gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texture1);
        gl.activeTexture(gl.TEXTURE1);
        //gl.bindTexture(gl.TEXTURE_2D, texture2);

	    // Do the drawing
        gl.drawElements(gl.TRIANGLES, triangleIndices_third.length, gl.UNSIGNED_BYTE, 0);
      
      
      
      
      
                          // Set up uniforms & attributes
        gl.uniformMatrix4fv(shaderProgram.MVmatrix,false,tMV4);
        gl.uniformMatrix4fv(shaderProgram.MVNormalmatrix,false,tMVn4);
        gl.uniformMatrix4fv(shaderProgram.MVPmatrix,false,tMVP4);
                 
        gl.bindBuffer(gl.ARRAY_BUFFER, trianglePosBuffer_fourth);
        gl.vertexAttribPointer(shaderProgram.PositionAttribute, trianglePosBuffer_fourth.itemSize,
          gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, triangleNormalBuffer_fourth);
        gl.vertexAttribPointer(shaderProgram.NormalAttribute, triangleNormalBuffer_fourth.itemSize,
          gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer_fourth);
        gl.vertexAttribPointer(shaderProgram.ColorAttribute, colorBuffer_fourth.itemSize,
          gl.FLOAT,false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, textureBuffer);
        gl.vertexAttribPointer(shaderProgram.texcoordAttribute, textureBuffer.itemSize,
          gl.FLOAT, false, 0, 0);

	    // Bind texture
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texture1);
        //gl.activeTexture(gl.TEXTURE1);
        //gl.bindTexture(gl.TEXTURE_2D, texture2);

	    // Do the drawing
        gl.drawElements(gl.TRIANGLES, triangleIndices_fourth.length, gl.UNSIGNED_BYTE, 0);

      
      
      
      
                          // Set up uniforms & attributes
        gl.uniformMatrix4fv(shaderProgram.MVmatrix,false,tMV5);
        gl.uniformMatrix4fv(shaderProgram.MVNormalmatrix,false,tMVn5);
        gl.uniformMatrix4fv(shaderProgram.MVPmatrix,false,tMVP5);
                 
        gl.bindBuffer(gl.ARRAY_BUFFER, trianglePosBuffer_fifth);
        gl.vertexAttribPointer(shaderProgram.PositionAttribute, trianglePosBuffer_fifth.itemSize,
          gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, triangleNormalBuffer_fifth);
        gl.vertexAttribPointer(shaderProgram.NormalAttribute, triangleNormalBuffer_fifth.itemSize,
          gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer_fifth);
        gl.vertexAttribPointer(shaderProgram.ColorAttribute, colorBuffer_fifth.itemSize,
          gl.FLOAT,false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, textureBuffer);
        gl.vertexAttribPointer(shaderProgram.texcoordAttribute, textureBuffer.itemSize,
          gl.FLOAT, false, 0, 0);

	    // Bind texture
        //gl.activeTexture(gl.TEXTURE0);
        //gl.bindTexture(gl.TEXTURE_2D, texture1);
        //gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D, texture2);

	    // Do the drawing
        gl.drawElements(gl.TRIANGLES, triangleIndices_fifth.length, gl.UNSIGNED_BYTE, 0);    
    }

    slider1.addEventListener("input",draw);
    slider2.addEventListener("input",draw);
    slider3.addEventListener("input",draw);
    slider4.addEventListener("input",draw);
    slider5.addEventListener("input",draw);
    initTextureThenDraw();
}
</script>
</body>

</html>
